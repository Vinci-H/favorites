import { app, BrowserWindow,Menu,screen,ipcMain,globalShortcut  } from 'electron'
const fs = require('fs')
const dialog = require('electron').dialog;

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`


ipcMain.on('min', e=> mainWindow.minimize());
ipcMain.on('max', e=> {
    if (mainWindow.isMaximized()) {
        mainWindow.unmaximize()
    } else {
        mainWindow.maximize()
    }
});
ipcMain.on('close', e=> mainWindow.close());

function createWindow () {
  // Menu.setApplicationMenu(null)
  /**
   * Initial window options
   */
  let size = screen.getPrimaryDisplay().workAreaSize
  let width = size.width
  let height = size.height
  mainWindow = new BrowserWindow({
    frame: false,
    // titleBarStyle: 'hidden',
    height: height,
    useContentSize: true,
    width: width,
    title:'数字胶州管理系统',
    icon:'../renderer/assets/logo.png',
    webPreferences: {
      nodeIntegration: true, // 使渲染进程拥有node环境
      webSecurity: false
    }
  })
  // mainWindow.setFullScreen(true) 
  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)
app.on('browser-window-focus', () => {
  globalShortcut.register('F5', () => {
    return false;
  })
})
app.on('browser-window-blur', () => {
  globalShortcut.unregister('F5')
})
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

ipcMain.on('close', (e)=> {
  app.exit()
})

//在主线程下，通过ipcMain对象监听渲染线程传过来的saveDialog事件
ipcMain.on('saveDialog', (event, arg) => {
  // 打开弹窗
  dialog.showSaveDialog(mainWindow, {
  // 在 Windows 和 Linux 上, 打开对话框不能同时是文件选择器和目录选择器, 因此如果在这些平台上将 properties 设置为["openFile"、"openDirectory"], 则将显示为目录选择器。
    properties: ['openFile', 'openDirectory'],
    // 默认情况下使用的绝对目录路径、绝对文件路径、文件名
    defaultPath: arg.fileName,
  // 点击保存回调
  }, filePath  =>{
    if(filePath){
      // filePath存在则为保存路径 否为undefined
      // 去掉头部无用字段并将base64转码成buffer
      let dataBuffer = Buffer.from(arg.baseCode.split('base64,')[1], 'base64')
      fs.writeFile(filePath, dataBuffer, err => {
        // 失败
        if (err) {
          // 向渲染进程发送消息通知失败
          mainWindow.webContents.send('defeatedDialog')
        }
      })
      // 成功 向渲染进程发送消息通知成功
      mainWindow.webContents.send('succeedDialog')
    }
  })
})

//在主线程下，通过ipcMain对象监听渲染线程传过来的saveDialog事件
ipcMain.on('saveDialogDocx', (event, arg) => {
  // 打开弹窗
  dialog.showSaveDialog(mainWindow, {
  // 在 Windows 和 Linux 上, 打开对话框不能同时是文件选择器和目录选择器, 因此如果在这些平台上将 properties 设置为["openFile"、"openDirectory"], 则将显示为目录选择器。
    properties: ['openFile', 'openDirectory'],
    // 默认情况下使用的绝对目录路径、绝对文件路径、文件名
    defaultPath: arg.fileName,
  // 点击保存回调
  }, filePath  =>{
    if(filePath){
      // filePath存在则为保存路径 否为undefined
      // 去掉头部无用字段并将base64转码成buffer
      let dataBuffer = Buffer.from(arg.baseCode.split('base64,')[1], 'base64')
      fs.writeFile(filePath, dataBuffer, err => {
        // 失败
        if (err) {
          // 向渲染进程发送消息通知失败
          mainWindow.webContents.send('defeatedDialogDocx')
        }
      })
      // 成功 向渲染进程发送消息通知成功
      mainWindow.webContents.send('succeedDialogDocx')
    }
  })
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
