import Vue from 'vue'
import axios from 'axios'
import Viewer from 'v-viewer';
import 'viewerjs/dist/viewer.css';
import App from './App'
import router from './router'
import store from './store'
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
Vue.use(Viewer, {
  defaultOptions: {
    zIndex: 9999, // 设置图片预览组件的层级，确保能在其他组件之上
  },
});
Vue.use(ViewUI);
if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
