import baseArea from '../data/base_area'
import baseStaff from '../data/base_staff'
import baseDict from '../data/base_dict'
import baseStaffDict from '../data/base_staff_dict'
let searchDataOld = {}
let resultOld = []
let searchAllDataOld = {}
let resultAllOld = []
export function list(searchData,page,limit){
    const baseStaffs = JSON.parse(JSON.stringify(baseStaff))
    let resultTrue = []
    if(JSON.stringify(searchData)!=JSON.stringify(searchDataOld)){
        console.log(searchData)
        searchDataOld = JSON.parse(JSON.stringify(searchData))
        const result = []
        for(const i of baseStaffs){
            let has = true
            for(const j in searchData){
                if(searchData[j]){
                    if(j=='user_name'){
                        if(i.user_name.indexOf(searchData[j])==-1){
                            has = false
                        }
                    }else if(j=='help_staff'){
                        if(i.help_staff.indexOf(searchData[j])==-1){
                            has = false
                        }
                    }else if(j=='related_staff_name'){
                        if(i.related_staff_name.indexOf(searchData[j])==-1){
                            has = false
                        }
                    }else if(j=='address'){
                        if(searchData[j].length==1){
                            if(i.street_id != searchData[j][0]){
                                has = false
                            }
                        }else if(searchData[j].length==2){
                            if(i.community_id != searchData[j][1]){
                                has = false
                            }
                        }
                    }else if(j=='staffTagIds1'||j=='staffTagIds2') {
                        let tagHas = false
                        for(const k of baseStaffDict){
                            if(i.id == k.staff_id&&searchData[j]==k.base_dict_id){
                                tagHas = true
                                break
                            }
                        }
                        if(!tagHas){
                            has = false
                        }
                    }else{
                        if(i[j]!=searchData[j]){
                            has = false
                        }
                    }
                }
            }
            if(has){
                result.push(i)
            }
        } 
        resultOld = result
        resultTrue = result.slice((page-1)*limit,(page-1)*limit+limit)        
    }else{
        resultTrue = resultOld.slice((page-1)*limit,(page-1)*limit+limit)   
    }
    for(const i of resultTrue){
        i.areaName = '胶州市'
        for(const j of baseArea){
            if(i.street_id==j.id){
                i.streetName = j.name
            }if(i.community_id==j.id){
                i.communityName = j.name
            }
        }
        if(i.create_time.indexOf('/')>-1){
            const create_time1 = i.create_time.split(' ')
            const create_time2 = create_time1[0].split('/')
            i.create_time = `${create_time2[2]}-${create_time2[1]}-${create_time2[0]} ${create_time1[1]}`
        }
        if(i.identity_type==1){
            i.identity_type_name='户主'
        }else{
            i.identity_type_name='户主家人'
        }
        if(i.party_member==1){
            i.party_member='是'
        }else{
            i.party_member='否'
        }
        if(i.deformed==1){
            i.deformed='是'
        }else{
            i.deformed='否'
        }
        if(i.marriage==1){
            i.marriage='未婚'
        }else if(i.marriage==2){
            i.marriage='已婚'
        }else if(i.marriage==3){
            i.marriage='离异'
        }else if(i.marriage==4){
            i.marriage='丧偶'
        }
        i.in_home_photo = Array.isArray(i.in_home_photo)?i.in_home_photo:i.in_home_photo.split(',')
        i.assess_photo = Array.isArray(i.assess_photo)?i.assess_photo:i.assess_photo.split(',')
        let difficulty = []
        for(const j of baseStaffDict){
            if(i.id == j.staff_id){
                for(const k of baseDict){
                    if(k.id==j.base_dict_id){
                        if(k.type=='STAFF_SITUA'){
                            i.type = k.name
                        }else if(k.type=='SITUATION'){
                            i.staffTagIds1 = k.name
                        }else if(k.type=='DIFFICULT_SITUATION'){
                            difficulty.push(k.name)
                        }
                    }
                }
            }
        }
        i.difficulty = difficulty.join(',')
    }
    return {
        list:resultTrue,
        total:resultOld.length
    }
}

export function listAll(searchData){
    let resultTrue = []
    const baseStaffs = JSON.parse(JSON.stringify(baseStaff))
    if(JSON.stringify(searchData)!=JSON.stringify(searchAllDataOld)){
        console.log(searchData)
        searchAllDataOld = JSON.parse(JSON.stringify(searchData))
        const result = []
        for(const i of baseStaffs){
            let has = true
            for(const j in searchData){
                if(searchData[j]){
                    if(j=='user_name'){
                        if(i.user_name.indexOf(searchData[j])==-1){
                            has = false
                        }
                    }else if(j=='help_staff'){
                        if(i.help_staff.indexOf(searchData[j])==-1){
                            has = false
                        }
                    }else if(j=='related_staff_name'){
                        if(i.related_staff_name.indexOf(searchData[j])==-1){
                            has = false
                        }
                    }else if(j=='address'){
                        if(searchData[j].length==1){
                            if(i.street_id != searchData[j][0]){
                                has = false
                            }
                        }else if(searchData[j].length==2){
                            if(i.community_id != searchData[j][1]){
                                has = false
                            }
                        }
                    }else if(j=='staffTagIds1'||j=='staffTagIds2') {
                        let tagHas = false
                        for(const k of baseStaffDict){
                            if(i.id == k.staff_id&&searchData[j]==k.base_dict_id){
                                tagHas = true
                                break
                            }
                        }
                        if(!tagHas){
                            has = false
                        }
                    }else{
                        if(i[j]!=searchData[j]){
                            has = false
                        }
                    }
                }
            }
            if(has){
                result.push(i)
            }
        } 
        resultAllOld = result
        resultTrue = JSON.parse(JSON.stringify(result))    
    }else{
        resultTrue = JSON.parse(JSON.stringify(resultAllOld))   
    }
    for(const i of resultTrue){
        i.areaName = '胶州市'
        for(const j of baseArea){
            if(i.street_id==j.id){
                i.streetName = j.name
            }if(i.community_id==j.id){
                i.communityName = j.name
            }
        }
        if(i.identity_type==1){
            i.identity_type_name='户主'
        }else{
            i.identity_type_name='户主家人'
        }
        if(i.party_member==1){
            i.party_member='是'
        }else{
            i.party_member='否'
        }
        if(i.deformed==1){
            i.deformed='是'
        }else{
            i.deformed='否'
        }
        if(i.marriage==1){
            i.marriage='未婚'
        }else if(i.marriage==2){
            i.marriage='已婚'
        }else if(i.marriage==3){
            i.marriage='离异'
        }else if(i.marriage==4){
            i.marriage='丧偶'
        }
        let difficulty = []
        for(const j of baseStaffDict){
            if(i.id == j.staff_id){
                for(const k of baseDict){
                    if(k.id==j.base_dict_id){
                        if(k.type=='STAFF_SITUA'){
                            i.type = k.name
                        }else if(k.type=='SITUATION'){
                            i.staffTagIds1 = k.name
                        }else if(k.type=='DIFFICULT_SITUATION'){
                            difficulty.push(k.name)
                        }
                    }
                }
            }
        }
        i.difficulty = difficulty.join(',')
    }
    return resultTrue
}