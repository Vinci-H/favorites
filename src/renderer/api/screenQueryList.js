import baseStaff from '../data/base_staff'
import baseDict from '../data/base_dict'
import baseStaffDict from '../data/base_staff_dict'
import baseArea from '../data/base_area'
export function screenQueryList(query){
  const staffs = []
  const staffs2 = []
  const staffs3 = []
  for(const i of baseStaff){
    if(i.community_id==query.communityId){
      i.homeAddress = i.home_address
      i.identityType = i.identity_type
      i.userName = i.user_name
      i.assessPhoto = i.assess_photo
      i.inHomePhoto = i.in_home_photo
      i.relatedStaffName = i.related_staff_name
      i.relatedStaffPhone = i.related_staff_phone
      i.partyMember = i.party_member
      i.marriage = i.marriage
      i.idCard = i.id_card
      i.streetId = i.street_id
      i.selfHelp = i.self_help
      i.helpStaffPhone = i.help_staff_phone
      i.helpStaff = i.help_staff
      i.helpMoney =i.help_money
      i.familySituation = i.family_situation
      i.familyNum = i.family_num
      i.ensurePopulation = i.ensure_population
      i.districtId = i.district_id
      i.difficultyDesc = i.difficulty_desc
      i.deformedType = i.deformed_type
      i.deformedLevel = i.deformed_level
      i.deformed = i.deformed
      i.createTime = i.create_time
      i.createBy = i.create_by
      i.communityId = i.community_id
      staffs.push(i)
    }
  }
  for(const i of staffs){
    i.staffDifficultyIds = []
    i.diffTagName = []
    for(const j of baseStaffDict){
      if(i.id==j.staff_id&&j.type=='DIFFICULT_SITUATION'){
        i.staffDifficultyIds.push(j.base_dict_id)
        for(const k of baseDict){
          if(j.base_dict_id==k.id){
            i.diffTagName.push(k.name)
            break
          }
        }
      }
    }
    i.diffTagName = i.diffTagName.join(',')
    if(query.admin){
      i.base_dict_id = []
      for(const j of baseStaffDict){
        if(i.id==j.staff_id&&(j.base_dict_id=='1643527922546966528'||j.base_dict_id=='1643528012837748736')){
          i.base_dict_id.push(j.base_dict_id)
          break
        }
      }
      for(const j of baseStaffDict){
        if(i.id==j.staff_id&&(j.base_dict_id!='1643527922546966528'||j.base_dict_id!='1643528012837748736')&&j.type!='DIFFICULT_SITUATION'){
          i.base_dict_id.push(j.base_dict_id)
          break
        }
      }
      for(const j of baseDict){
        if(i.base_dict_id[0]==j.id){
          i.adminTagName = j.name
        }
        if(i.base_dict_id[1]==j.id){
          i.tagName = j.name
          i.tagIcon = j.icon
        }
      }
      if(i.base_dict_id.length>1){
        i.idCard = '******************'
        staffs2.push(i)
      }
    }else{
      i.base_dict_id = []
      for(const j of baseStaffDict){
        if(i.id==j.staff_id&&(j.base_dict_id=='1643527922546966528'||j.base_dict_id=='1643528012837748736')){
          i.base_dict_id.push(j.base_dict_id)
          break
        }
      }
      for(const j of baseStaffDict){
        if(i.id==j.staff_id&&(j.base_dict_id!='1643527922546966528'||j.base_dict_id!='1643528012837748736')&&j.type!='DIFFICULT_SITUATION'){
          i.base_dict_id.push(j.base_dict_id)
          break
        }
      }
      for(const j of baseDict){
        if(i.base_dict_id[0]==j.id){
          i.tagName = j.name
          i.tagIcon = j.icon
        }
      }
      staffs2.push(i)
    }
  }
  for(const i of staffs2){
    for(const j of baseArea){
      if(i.districtId==j.id){
        i.districtName = j.name
      }
      if(i.streetId==j.id){
        i.streetName = j.name
      }
      if(i.communityId==j.id){
        i.communityName = j.name
      }
    }
  }
  if(!query.diffTagId&&!query.tagId){
    console.log(staffs2)
    return {
      rows:staffs2,
      total:staffs2.length
    }
  }else if(query.diffTagId&&!query.tagId){
    for(const i of staffs2){
      if(i.staffDifficultyIds.indexOf(query.diffTagId)>-1){
        staffs3.push(i)
      }
    }
    return {
      rows:staffs3,
      total:staffs3.length
    }
  }else if(!query.diffTagId&&query.tagId){
    for(const i of staffs2){
      if(i.base_dict_id.indexOf(query.tagId)>-1){
        staffs3.push(i)
      }
    }
    return {
      rows:staffs3,
      total:staffs3.length
    }
  }else if(query.diffTagId&&query.tagId){
    for(const i of staffs2){
      if(i.staffDifficultyIds.indexOf(query.diffTagId)>-1&&i.base_dict_id.indexOf(query.tagId)>-1){
        staffs3.push(i)
      }
    }
    return {
      rows:staffs3,
      total:staffs3.length
    }
  }
}
