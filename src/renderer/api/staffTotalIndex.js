export function staffTotalIndex(query){
  console.log('staffTotalIndex',query)
    return {
        streets:[
          {
            "areaId": "370281001000",
            "name": "阜安街道",
            "total": "165"
          },
          {
            "areaId": "370281002000",
            "name": "中云街道",
            "total": "193"
          },
          {
            "areaId": "370281004000",
            "name": "三里河街道",
            "total": "444"
          },
          {
            "areaId": "370281006000",
            "name": "九龙街道",
            "total": "1152"
          },
          {
            "areaId": "370281007000",
            "name": "胶东街道",
            "total": "856"
          },
          {
            "areaId": "370281008000",
            "name": "胶北街道",
            "total": "1126"
          },
          {
            "areaId": "370281009000",
            "name": "胶西街道",
            "total": "2347"
          },
          {
            "areaId": "370281010000",
            "name": "胶莱街道",
            "total": "1843"
          },
          {
            "areaId": "370281102000",
            "name": "李哥庄镇",
            "total": "955"
          },
          {
            "areaId": "370281105000",
            "name": "铺集镇",
            "total": "1330"
          },
          {
            "areaId": "370281108000",
            "name": "里岔镇",
            "total": "1369"
          },
          {
            "areaId": "370281111000",
            "name": "洋河镇",
            "total": "1113"
          }
        ],
        total: "12893"
    }
}