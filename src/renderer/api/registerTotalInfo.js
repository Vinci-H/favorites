import baseStaff from '../data/base_staff'
import baseDict from '../data/base_dict'
import baseStaffDict from '../data/base_staff_dict'
export function registerTotalInfo(query){
  console.log('registerTotalInfo',query)
    if(query.districtId == '370281000000'){
        return {
          "registerMaster": "12893",
          "register": "13728",
          "registerDiffVos": [
            {
              "name": "医疗支出过大",
              "id": "1643527717109956608",
              "total": "668"
            },
            {
              "name": "教育支出过大",
              "id": "1643527895678255104",
              "total": "32"
            },
            {
              "name": "残疾家庭",
              "id": "1644931344399663104",
              "total": "3210"
            },
            {
              "name": "服刑以及刑满人员",
              "id": "1644931361000718336",
              "total": "0"
            },
            {
              "name": "家庭缺少劳动力",
              "id": "1644931392051150848",
              "total": "988"
            },
            {
              "name": "已办理低保",
              "id": "1723937541284163584",
              "total": "0"
            },
            {
              "name": "已办理特困",
              "id": "1723937574104592384",
              "total": "0"
            },
            {
              "name": "已办理临时救助",
              "id": "1723937612713160704",
              "total": "0"
            },
            {
              "name": "不符合救助",
              "id": "1723937634187997184",
              "total": "0"
            }
          ],
          "registerTagVos": [
            {
              "name": "低保",
              "id": "1643527543906172928",
              "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
              "total": "6441"
            },
            {
              "name": "特困",
              "id": "1643527629604192256",
              "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
              "total": "1446"
            },
            {
              "name": "困难家庭户",
              "id": "1644930428288172032",
              "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
              "total": "5006"
            },
            {
              "name": "普通户",
              "id": "1644930999011311616",
              "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
              "total": "0"
            },
            {
              "name": "困难家庭户(已办理低保）",
              "id": "1687620234333384704",
              "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
              "total": "0"
            },
            {
              "name": "困难家庭户(已办理特困）",
              "id": "1723888725067558912",
              "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
              "total": "0"
            },
            {
              "name": "困难家庭户(已办理临时救助）",
              "id": "1723888815937155072",
              "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
              "total": "0"
            },
            {
              "name": "困难家庭户(不符合救助）",
              "id": "1723888881116639232",
              "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
              "total": "0"
            }
          ],
          "registerTagTotal": "12893",
          "communityCount": "204"
        }
    }
    if(query.streetId == '370281010000'){
      return {
        "registerMaster": "1843",
        "register": "1953",
        "registerDiffVos": [
          {
            "name": "医疗支出过大",
            "id": "1643527717109956608",
            "total": "64"
          },
          {
            "name": "教育支出过大",
            "id": "1643527895678255104",
            "total": "1"
          },
          {
            "name": "残疾家庭",
            "id": "1644931344399663104",
            "total": "675"
          },
          {
            "name": "服刑以及刑满人员",
            "id": "1644931361000718336",
            "total": "0"
          },
          {
            "name": "家庭缺少劳动力",
            "id": "1644931392051150848",
            "total": "141"
          },
          {
            "name": "已办理低保",
            "id": "1723937541284163584",
            "total": "0"
          },
          {
            "name": "已办理特困",
            "id": "1723937574104592384",
            "total": "0"
          },
          {
            "name": "已办理临时救助",
            "id": "1723937612713160704",
            "total": "0"
          },
          {
            "name": "不符合救助",
            "id": "1723937634187997184",
            "total": "0"
          }
        ],
        "registerTagVos": [
          {
            "name": "低保",
            "id": "1643527543906172928",
            "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
            "total": "801"
          },
          {
            "name": "特困",
            "id": "1643527629604192256",
            "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
            "total": "160"
          },
          {
            "name": "困难家庭户",
            "id": "1644930428288172032",
            "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
            "total": "882"
          },
          {
            "name": "普通户",
            "id": "1644930999011311616",
            "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理低保）",
            "id": "1687620234333384704",
            "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理特困）",
            "id": "1723888725067558912",
            "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理临时救助）",
            "id": "1723888815937155072",
            "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(不符合救助）",
            "id": "1723888881116639232",
            "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
            "total": "0"
          }
        ],
        "registerTagTotal": "1843",
        "communityCount": "16"
      }
    }
    if(query.streetId == '370281102000'){
      return {
        "registerMaster": "955",
        "register": "999",
        "registerDiffVos": [
          {
            "name": "医疗支出过大",
            "id": "1643527717109956608",
            "total": "66"
          },
          {
            "name": "教育支出过大",
            "id": "1643527895678255104",
            "total": "1"
          },
          {
            "name": "残疾家庭",
            "id": "1644931344399663104",
            "total": "188"
          },
          {
            "name": "服刑以及刑满人员",
            "id": "1644931361000718336",
            "total": "0"
          },
          {
            "name": "家庭缺少劳动力",
            "id": "1644931392051150848",
            "total": "49"
          },
          {
            "name": "已办理低保",
            "id": "1723937541284163584",
            "total": "0"
          },
          {
            "name": "已办理特困",
            "id": "1723937574104592384",
            "total": "0"
          },
          {
            "name": "已办理临时救助",
            "id": "1723937612713160704",
            "total": "0"
          },
          {
            "name": "不符合救助",
            "id": "1723937634187997184",
            "total": "0"
          }
        ],
        "registerTagVos": [
          {
            "name": "低保",
            "id": "1643527543906172928",
            "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
            "total": "559"
          },
          {
            "name": "特困",
            "id": "1643527629604192256",
            "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
            "total": "47"
          },
          {
            "name": "困难家庭户",
            "id": "1644930428288172032",
            "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
            "total": "349"
          },
          {
            "name": "普通户",
            "id": "1644930999011311616",
            "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理低保）",
            "id": "1687620234333384704",
            "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理特困）",
            "id": "1723888725067558912",
            "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理临时救助）",
            "id": "1723888815937155072",
            "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(不符合救助）",
            "id": "1723888881116639232",
            "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
            "total": "0"
          }
        ],
        "registerTagTotal": "955",
        "communityCount": "8"
      }
    }
    if(query.streetId == '370281007000'){
      return {
        "registerMaster": "856",
        "register": "900",
        "registerDiffVos": [
          {
            "name": "医疗支出过大",
            "id": "1643527717109956608",
            "total": "46"
          },
          {
            "name": "教育支出过大",
            "id": "1643527895678255104",
            "total": "3"
          },
          {
            "name": "残疾家庭",
            "id": "1644931344399663104",
            "total": "94"
          },
          {
            "name": "服刑以及刑满人员",
            "id": "1644931361000718336",
            "total": "0"
          },
          {
            "name": "家庭缺少劳动力",
            "id": "1644931392051150848",
            "total": "49"
          },
          {
            "name": "已办理低保",
            "id": "1723937541284163584",
            "total": "0"
          },
          {
            "name": "已办理特困",
            "id": "1723937574104592384",
            "total": "0"
          },
          {
            "name": "已办理临时救助",
            "id": "1723937612713160704",
            "total": "0"
          },
          {
            "name": "不符合救助",
            "id": "1723937634187997184",
            "total": "0"
          }
        ],
        "registerTagVos": [
          {
            "name": "低保",
            "id": "1643527543906172928",
            "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
            "total": "559"
          },
          {
            "name": "特困",
            "id": "1643527629604192256",
            "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
            "total": "47"
          },
          {
            "name": "困难家庭户",
            "id": "1644930428288172032",
            "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
            "total": "250"
          },
          {
            "name": "普通户",
            "id": "1644930999011311616",
            "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理低保）",
            "id": "1687620234333384704",
            "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理特困）",
            "id": "1723888725067558912",
            "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理临时救助）",
            "id": "1723888815937155072",
            "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(不符合救助）",
            "id": "1723888881116639232",
            "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
            "total": "0"
          }
        ],
        "registerTagTotal": "856",
        "communityCount": "12"
      }
    }
    if(query.streetId == '370281008000'){
      return {
        "registerMaster": "1126",
        "register": "1198",
        "registerDiffVos": [
          {
            "name": "医疗支出过大",
            "id": "1643527717109956608",
            "total": "24"
          },
          {
            "name": "教育支出过大",
            "id": "1643527895678255104",
            "total": "3"
          },
          {
            "name": "残疾家庭",
            "id": "1644931344399663104",
            "total": "438"
          },
          {
            "name": "服刑以及刑满人员",
            "id": "1644931361000718336",
            "total": "0"
          },
          {
            "name": "家庭缺少劳动力",
            "id": "1644931392051150848",
            "total": "52"
          },
          {
            "name": "已办理低保",
            "id": "1723937541284163584",
            "total": "0"
          },
          {
            "name": "已办理特困",
            "id": "1723937574104592384",
            "total": "0"
          },
          {
            "name": "已办理临时救助",
            "id": "1723937612713160704",
            "total": "0"
          },
          {
            "name": "不符合救助",
            "id": "1723937634187997184",
            "total": "0"
          }
        ],
        "registerTagVos": [
          {
            "name": "低保",
            "id": "1643527543906172928",
            "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
            "total": "487"
          },
          {
            "name": "特困",
            "id": "1643527629604192256",
            "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
            "total": "122"
          },
          {
            "name": "困难家庭户",
            "id": "1644930428288172032",
            "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
            "total": "517"
          },
          {
            "name": "普通户",
            "id": "1644930999011311616",
            "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理低保）",
            "id": "1687620234333384704",
            "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理特困）",
            "id": "1723888725067558912",
            "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理临时救助）",
            "id": "1723888815937155072",
            "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(不符合救助）",
            "id": "1723888881116639232",
            "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
            "total": "0"
          }
        ],
        "registerTagTotal": "1126",
        "communityCount": "19"
      }
    }
    if(query.streetId == '370281008000'){
      return {
        "registerMaster": "1118",
        "register": "1190",
        "registerDiffVos": [
          {
            "name": "医疗支出过大",
            "id": "1643527717109956608",
            "total": "24"
          },
          {
            "name": "教育支出过大",
            "id": "1643527895678255104",
            "total": "3"
          },
          {
            "name": "残疾家庭",
            "id": "1644931344399663104",
            "total": "439"
          },
          {
            "name": "服刑以及刑满人员",
            "id": "1644931361000718336",
            "total": "0"
          },
          {
            "name": "家庭缺少劳动力",
            "id": "1644931392051150848",
            "total": "52"
          },
          {
            "name": "已办理低保",
            "id": "1723937541284163584",
            "total": "0"
          },
          {
            "name": "已办理特困",
            "id": "1723937574104592384",
            "total": "0"
          },
          {
            "name": "已办理临时救助",
            "id": "1723937612713160704",
            "total": "0"
          },
          {
            "name": "不符合救助",
            "id": "1723937634187997184",
            "total": "0"
          }
        ],
        "registerTagVos": [
          {
            "name": "低保",
            "id": "1643527543906172928",
            "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
            "total": "477"
          },
          {
            "name": "特困",
            "id": "1643527629604192256",
            "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
            "total": "124"
          },
          {
            "name": "困难家庭户",
            "id": "1644930428288172032",
            "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
            "total": "517"
          },
          {
            "name": "普通户",
            "id": "1644930999011311616",
            "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理低保）",
            "id": "1687620234333384704",
            "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理特困）",
            "id": "1723888725067558912",
            "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理临时救助）",
            "id": "1723888815937155072",
            "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(不符合救助）",
            "id": "1723888881116639232",
            "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
            "total": "0"
          }
        ],
        "registerTagTotal": "1118",
        "communityCount": "19"
      }
    }
    if(query.streetId == '370281001000'){
      return {
        "registerMaster": "165",
        "register": "195",
        "registerDiffVos": [
          {
            "name": "医疗支出过大",
            "id": "1643527717109956608",
            "total": "0"
          },
          {
            "name": "教育支出过大",
            "id": "1643527895678255104",
            "total": "0"
          },
          {
            "name": "残疾家庭",
            "id": "1644931344399663104",
            "total": "1"
          },
          {
            "name": "服刑以及刑满人员",
            "id": "1644931361000718336",
            "total": "0"
          },
          {
            "name": "家庭缺少劳动力",
            "id": "1644931392051150848",
            "total": "1"
          },
          {
            "name": "已办理低保",
            "id": "1723937541284163584",
            "total": "0"
          },
          {
            "name": "已办理特困",
            "id": "1723937574104592384",
            "total": "0"
          },
          {
            "name": "已办理临时救助",
            "id": "1723937612713160704",
            "total": "0"
          },
          {
            "name": "不符合救助",
            "id": "1723937634187997184",
            "total": "0"
          }
        ],
        "registerTagVos": [
          {
            "name": "低保",
            "id": "1643527543906172928",
            "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
            "total": "138"
          },
          {
            "name": "特困",
            "id": "1643527629604192256",
            "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
            "total": "25"
          },
          {
            "name": "困难家庭户",
            "id": "1644930428288172032",
            "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
            "total": "2"
          },
          {
            "name": "普通户",
            "id": "1644930999011311616",
            "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理低保）",
            "id": "1687620234333384704",
            "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理特困）",
            "id": "1723888725067558912",
            "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理临时救助）",
            "id": "1723888815937155072",
            "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(不符合救助）",
            "id": "1723888881116639232",
            "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
            "total": "0"
          }
        ],
        "registerTagTotal": "165",
        "communityCount": "15"
      }
    }
    if(query.streetId == '370281002000'){
      return {
        "registerMaster": "193",
        "register": "228",
        "registerDiffVos": [
          {
            "name": "医疗支出过大",
            "id": "1643527717109956608",
            "total": "0"
          },
          {
            "name": "教育支出过大",
            "id": "1643527895678255104",
            "total": "0"
          },
          {
            "name": "残疾家庭",
            "id": "1644931344399663104",
            "total": "1"
          },
          {
            "name": "服刑以及刑满人员",
            "id": "1644931361000718336",
            "total": "0"
          },
          {
            "name": "家庭缺少劳动力",
            "id": "1644931392051150848",
            "total": "0"
          },
          {
            "name": "已办理低保",
            "id": "1723937541284163584",
            "total": "0"
          },
          {
            "name": "已办理特困",
            "id": "1723937574104592384",
            "total": "0"
          },
          {
            "name": "已办理临时救助",
            "id": "1723937612713160704",
            "total": "0"
          },
          {
            "name": "不符合救助",
            "id": "1723937634187997184",
            "total": "0"
          }
        ],
        "registerTagVos": [
          {
            "name": "低保",
            "id": "1643527543906172928",
            "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
            "total": "155"
          },
          {
            "name": "特困",
            "id": "1643527629604192256",
            "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
            "total": "37"
          },
          {
            "name": "困难家庭户",
            "id": "1644930428288172032",
            "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
            "total": "1"
          },
          {
            "name": "普通户",
            "id": "1644930999011311616",
            "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理低保）",
            "id": "1687620234333384704",
            "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理特困）",
            "id": "1723888725067558912",
            "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理临时救助）",
            "id": "1723888815937155072",
            "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(不符合救助）",
            "id": "1723888881116639232",
            "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
            "total": "0"
          }
        ],
        "registerTagTotal": "193",
        "communityCount": "21"
      }
    }
    if(query.streetId == '370281004000'){
      return {
        "registerMaster": "444",
        "register": "470",
        "registerDiffVos": [
          {
            "name": "医疗支出过大",
            "id": "1643527717109956608",
            "total": "9"
          },
          {
            "name": "教育支出过大",
            "id": "1643527895678255104",
            "total": "0"
          },
          {
            "name": "残疾家庭",
            "id": "1644931344399663104",
            "total": "39"
          },
          {
            "name": "服刑以及刑满人员",
            "id": "1644931361000718336",
            "total": "0"
          },
          {
            "name": "家庭缺少劳动力",
            "id": "1644931392051150848",
            "total": "19"
          },
          {
            "name": "已办理低保",
            "id": "1723937541284163584",
            "total": "0"
          },
          {
            "name": "已办理特困",
            "id": "1723937574104592384",
            "total": "0"
          },
          {
            "name": "已办理临时救助",
            "id": "1723937612713160704",
            "total": "0"
          },
          {
            "name": "不符合救助",
            "id": "1723937634187997184",
            "total": "0"
          }
        ],
        "registerTagVos": [
          {
            "name": "低保",
            "id": "1643527543906172928",
            "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
            "total": "291"
          },
          {
            "name": "特困",
            "id": "1643527629604192256",
            "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
            "total": "78"
          },
          {
            "name": "困难家庭户",
            "id": "1644930428288172032",
            "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
            "total": "75"
          },
          {
            "name": "普通户",
            "id": "1644930999011311616",
            "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理低保）",
            "id": "1687620234333384704",
            "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理特困）",
            "id": "1723888725067558912",
            "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理临时救助）",
            "id": "1723888815937155072",
            "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(不符合救助）",
            "id": "1723888881116639232",
            "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
            "total": "0"
          }
        ],
        "registerTagTotal": "444",
        "communityCount": "23"
      }
    }
    if(query.streetId == '370281009000'){
      return {
        "registerMaster": "2347",
        "register": "2463",
        "registerDiffVos": [
          {
            "name": "医疗支出过大",
            "id": "1643527717109956608",
            "total": "134"
          },
          {
            "name": "教育支出过大",
            "id": "1643527895678255104",
            "total": "6"
          },
          {
            "name": "残疾家庭",
            "id": "1644931344399663104",
            "total": "823"
          },
          {
            "name": "服刑以及刑满人员",
            "id": "1644931361000718336",
            "total": "0"
          },
          {
            "name": "家庭缺少劳动力",
            "id": "1644931392051150848",
            "total": "267"
          },
          {
            "name": "已办理低保",
            "id": "1723937541284163584",
            "total": "0"
          },
          {
            "name": "已办理特困",
            "id": "1723937574104592384",
            "total": "0"
          },
          {
            "name": "已办理临时救助",
            "id": "1723937612713160704",
            "total": "0"
          },
          {
            "name": "不符合救助",
            "id": "1723937634187997184",
            "total": "0"
          }
        ],
        "registerTagVos": [
          {
            "name": "低保",
            "id": "1643527543906172928",
            "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
            "total": "1003"
          },
          {
            "name": "特困",
            "id": "1643527629604192256",
            "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
            "total": "114"
          },
          {
            "name": "困难家庭户",
            "id": "1644930428288172032",
            "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
            "total": "1230"
          },
          {
            "name": "普通户",
            "id": "1644930999011311616",
            "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理低保）",
            "id": "1687620234333384704",
            "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理特困）",
            "id": "1723888725067558912",
            "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理临时救助）",
            "id": "1723888815937155072",
            "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(不符合救助）",
            "id": "1723888881116639232",
            "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
            "total": "0"
          }
        ],
        "registerTagTotal": "2347",
        "communityCount": "21"
      }
    }
    if(query.streetId == '370281006000'){
      return {
        "registerMaster": "1152",
        "register": "1241",
        "registerDiffVos": [
          {
            "name": "医疗支出过大",
            "id": "1643527717109956608",
            "total": "44"
          },
          {
            "name": "教育支出过大",
            "id": "1643527895678255104",
            "total": "8"
          },
          {
            "name": "残疾家庭",
            "id": "1644931344399663104",
            "total": "253"
          },
          {
            "name": "服刑以及刑满人员",
            "id": "1644931361000718336",
            "total": "0"
          },
          {
            "name": "家庭缺少劳动力",
            "id": "1644931392051150848",
            "total": "93"
          },
          {
            "name": "已办理低保",
            "id": "1723937541284163584",
            "total": "0"
          },
          {
            "name": "已办理特困",
            "id": "1723937574104592384",
            "total": "0"
          },
          {
            "name": "已办理临时救助",
            "id": "1723937612713160704",
            "total": "0"
          },
          {
            "name": "不符合救助",
            "id": "1723937634187997184",
            "total": "0"
          }
        ],
        "registerTagVos": [
          {
            "name": "低保",
            "id": "1643527543906172928",
            "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
            "total": "604"
          },
          {
            "name": "特困",
            "id": "1643527629604192256",
            "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
            "total": "152"
          },
          {
            "name": "困难家庭户",
            "id": "1644930428288172032",
            "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
            "total": "396"
          },
          {
            "name": "普通户",
            "id": "1644930999011311616",
            "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理低保）",
            "id": "1687620234333384704",
            "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理特困）",
            "id": "1723888725067558912",
            "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理临时救助）",
            "id": "1723888815937155072",
            "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(不符合救助）",
            "id": "1723888881116639232",
            "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
            "total": "0"
          }
        ],
        "registerTagTotal": "1152",
        "communityCount": "21"
      }
    }
    if(query.streetId == '370281111000'){
      return {
        "registerMaster": "1113",
        "register": "1202",
        "registerDiffVos": [
          {
            "name": "医疗支出过大",
            "id": "1643527717109956608",
            "total": "60"
          },
          {
            "name": "教育支出过大",
            "id": "1643527895678255104",
            "total": "3"
          },
          {
            "name": "残疾家庭",
            "id": "1644931344399663104",
            "total": "74"
          },
          {
            "name": "服刑以及刑满人员",
            "id": "1644931361000718336",
            "total": "0"
          },
          {
            "name": "家庭缺少劳动力",
            "id": "1644931392051150848",
            "total": "80"
          },
          {
            "name": "已办理低保",
            "id": "1723937541284163584",
            "total": "0"
          },
          {
            "name": "已办理特困",
            "id": "1723937574104592384",
            "total": "0"
          },
          {
            "name": "已办理临时救助",
            "id": "1723937612713160704",
            "total": "0"
          },
          {
            "name": "不符合救助",
            "id": "1723937634187997184",
            "total": "0"
          }
        ],
        "registerTagVos": [
          {
            "name": "低保",
            "id": "1643527543906172928",
            "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
            "total": "708"
          },
          {
            "name": "特困",
            "id": "1643527629604192256",
            "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
            "total": "188"
          },
          {
            "name": "困难家庭户",
            "id": "1644930428288172032",
            "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
            "total": "217"
          },
          {
            "name": "普通户",
            "id": "1644930999011311616",
            "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理低保）",
            "id": "1687620234333384704",
            "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理特困）",
            "id": "1723888725067558912",
            "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理临时救助）",
            "id": "1723888815937155072",
            "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(不符合救助）",
            "id": "1723888881116639232",
            "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
            "total": "0"
          }
        ],
        "registerTagTotal": "1113",
        "communityCount": "17"
      }
    }
    if(query.streetId == '370281108000'){
      return {
        "registerMaster": "1369",
        "register": "1477",
        "registerDiffVos": [
          {
            "name": "医疗支出过大",
            "id": "1643527717109956608",
            "total": "136"
          },
          {
            "name": "教育支出过大",
            "id": "1643527895678255104",
            "total": "4"
          },
          {
            "name": "残疾家庭",
            "id": "1644931344399663104",
            "total": "228"
          },
          {
            "name": "服刑以及刑满人员",
            "id": "1644931361000718336",
            "total": "0"
          },
          {
            "name": "家庭缺少劳动力",
            "id": "1644931392051150848",
            "total": "146"
          },
          {
            "name": "已办理低保",
            "id": "1723937541284163584",
            "total": "0"
          },
          {
            "name": "已办理特困",
            "id": "1723937574104592384",
            "total": "0"
          },
          {
            "name": "已办理临时救助",
            "id": "1723937612713160704",
            "total": "0"
          },
          {
            "name": "不符合救助",
            "id": "1723937634187997184",
            "total": "0"
          }
        ],
        "registerTagVos": [
          {
            "name": "低保",
            "id": "1643527543906172928",
            "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
            "total": "607"
          },
          {
            "name": "特困",
            "id": "1643527629604192256",
            "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
            "total": "248"
          },
          {
            "name": "困难家庭户",
            "id": "1644930428288172032",
            "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
            "total": "514"
          },
          {
            "name": "普通户",
            "id": "1644930999011311616",
            "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理低保）",
            "id": "1687620234333384704",
            "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理特困）",
            "id": "1723888725067558912",
            "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理临时救助）",
            "id": "1723888815937155072",
            "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(不符合救助）",
            "id": "1723888881116639232",
            "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
            "total": "0"
          }
        ],
        "registerTagTotal": "1369",
        "communityCount": "12"
      }
    }
    if(query.streetId == '370281105000'){
      return {
        "registerMaster": "1330",
        "register": "1402",
        "registerDiffVos": [
          {
            "name": "医疗支出过大",
            "id": "1643527717109956608",
            "total": "85"
          },
          {
            "name": "教育支出过大",
            "id": "1643527895678255104",
            "total": "3"
          },
          {
            "name": "残疾家庭",
            "id": "1644931344399663104",
            "total": "396"
          },
          {
            "name": "服刑以及刑满人员",
            "id": "1644931361000718336",
            "total": "0"
          },
          {
            "name": "家庭缺少劳动力",
            "id": "1644931392051150848",
            "total": "91"
          },
          {
            "name": "已办理低保",
            "id": "1723937541284163584",
            "total": "0"
          },
          {
            "name": "已办理特困",
            "id": "1723937574104592384",
            "total": "0"
          },
          {
            "name": "已办理临时救助",
            "id": "1723937612713160704",
            "total": "0"
          },
          {
            "name": "不符合救助",
            "id": "1723937634187997184",
            "total": "0"
          }
        ],
        "registerTagVos": [
          {
            "name": "低保",
            "id": "1643527543906172928",
            "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
            "total": "529"
          },
          {
            "name": "特困",
            "id": "1643527629604192256",
            "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
            "total": "228"
          },
          {
            "name": "困难家庭户",
            "id": "1644930428288172032",
            "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
            "total": "573"
          },
          {
            "name": "普通户",
            "id": "1644930999011311616",
            "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理低保）",
            "id": "1687620234333384704",
            "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理特困）",
            "id": "1723888725067558912",
            "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理临时救助）",
            "id": "1723888815937155072",
            "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(不符合救助）",
            "id": "1723888881116639232",
            "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
            "total": "0"
          }
        ],
        "registerTagTotal": "1330",
        "communityCount": "19"
      }
    }
    if(query.communityId){
      const staffs = []
      const result = {
        communityCount:1,
        register:'',
        registerDiffVos:[
          {
            "name": "医疗支出过大",
            "id": "1643527717109956608",
            "total": "668"
          },
          {
            "name": "教育支出过大",
            "id": "1643527895678255104",
            "total": "32"
          },
          {
            "name": "残疾家庭",
            "id": "1644931344399663104",
            "total": "3210"
          },
          {
            "name": "服刑以及刑满人员",
            "id": "1644931361000718336",
            "total": "0"
          },
          {
            "name": "家庭缺少劳动力",
            "id": "1644931392051150848",
            "total": "988"
          },
          {
            "name": "已办理低保",
            "id": "1723937541284163584",
            "total": "0"
          },
          {
            "name": "已办理特困",
            "id": "1723937574104592384",
            "total": "0"
          },
          {
            "name": "已办理临时救助",
            "id": "1723937612713160704",
            "total": "0"
          },
          {
            "name": "不符合救助",
            "id": "1723937634187997184",
            "total": "0"
          }
        ],
        registerMaster:'',
        registerTagTotal:12893,
        registerTagVos:[
          {
            "name": "低保",
            "id": "1643527543906172928",
            "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png",
            "total": "51"
          },
          {
            "name": "特困",
            "id": "1643527629604192256",
            "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png",
            "total": "3"
          },
          {
            "name": "困难家庭户",
            "id": "1644930428288172032",
            "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png",
            "total": "82"
          },
          {
            "name": "普通户",
            "id": "1644930999011311616",
            "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理低保）",
            "id": "1687620234333384704",
            "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理特困）",
            "id": "1723888725067558912",
            "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(已办理临时救助）",
            "id": "1723888815937155072",
            "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png",
            "total": "0"
          },
          {
            "name": "困难家庭户(不符合救助）",
            "id": "1723888881116639232",
            "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png",
            "total": "0"
          }
        ]
      }
      for(const i of baseStaff){
        if(i.community_id==query.communityId){
          staffs.push(i)
        }
      }
      for(const i of staffs){
        for(const j of baseStaffDict){
          if(i.id==j.staff_id&&j.type!='DIFFICULT_SITUATION'){
            i.base_dict_id = j.base_dict_id
            break
          }
        }
      }
      let registerMaster = 0
      result.register = staffs.length.toString()
      for(const i of staffs){
        if(i.identity_type==1){
          registerMaster++
        }
      }
      result.registerMaster = registerMaster.toString()
      for(const i of result.registerTagVos){
        let total = 0
        for(const j of staffs){
          if(i.id==j.base_dict_id){
            total++
          }
        }
        i.total = total.toString()
      }
      return result
    }
}