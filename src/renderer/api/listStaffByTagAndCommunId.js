import baseStaff from '../data/base_staff'
import baseDict from '../data/base_dict'
import baseStaffDict from '../data/base_staff_dict'
export function listStaffByTagAndCommunId(query){
  console.log(query)
  const staffs = []
  const familySitua = [
    {
      "createBy": "admin",
      "createTime": "2023-04-05 16:14:36",
      "updateBy": "18562716599",
      "updateTime": "2023-05-09 08:29:21",
      "id": "1643527543906172928",
      "name": "低保",
      "type": "SITUATION",
      "icon": "https://cdn.files.ordosszcs.com/20230409/3b2386cbe1434f8b887aec460455e315.png"
    },
    {
      "createBy": "admin",
      "createTime": "2023-04-05 16:14:57",
      "updateBy": "18562716599",
      "updateTime": "2023-05-09 08:29:25",
      "id": "1643527629604192256",
      "name": "特困",
      "type": "SITUATION",
      "icon": "https://cdn.files.ordosszcs.com/20230409/933720f2614642cc8cd370d5ef0bf537.png"
    },
    {
      "createBy": "admin",
      "createTime": "2023-04-09 13:09:10",
      "updateBy": "admin",
      "updateTime": "2023-04-09 13:35:05",
      "id": "1644930428288172032",
      "name": "困难家庭户",
      "type": "SITUATION",
      "icon": "https://cdn.files.ordosszcs.com/20230409/c6b652fe5cdb4cb8b41e25a88a9fe877.png"
    },
    {
      "createBy": "admin",
      "createTime": "2023-04-09 13:11:26",
      "updateBy": "admin",
      "updateTime": "2023-04-09 13:35:12",
      "id": "1644930999011311616",
      "name": "普通户",
      "type": "SITUATION",
      "icon": "https://cdn.files.ordosszcs.com/20230409/056de37e8f82472aa5d45160a8d19731.png"
    },
    {
      "createBy": "admin",
      "createTime": "2023-08-05 08:23:13",
      "updateBy": "admin",
      "updateTime": "2023-11-13 10:21:21",
      "id": "1687620234333384704",
      "name": "困难家庭户(已办理低保）",
      "type": "SITUATION",
      "icon": "https://cdn.files.ordosszcs.com/20231113/8b590b6a353749d090bbe663f360a803.png"
    },
    {
      "createBy": "admin",
      "createTime": "2023-11-13 10:21:15",
      "updateBy": "",
      "id": "1723888725067558912",
      "name": "困难家庭户(已办理特困）",
      "type": "SITUATION",
      "icon": "https://cdn.files.ordosszcs.com/20231113/c386839423fe4b8a94c66429383dbf63.png"
    },
    {
      "createBy": "admin",
      "createTime": "2023-11-13 10:21:37",
      "updateBy": "",
      "id": "1723888815937155072",
      "name": "困难家庭户(已办理临时救助）",
      "type": "SITUATION",
      "icon": "https://cdn.files.ordosszcs.com/20231113/dbef9a21d40e47abb4adc89d825d8506.png"
    },
    {
      "createBy": "admin",
      "createTime": "2023-11-13 10:21:52",
      "updateBy": "",
      "id": "1723888881116639232",
      "name": "困难家庭户(不符合救助）",
      "type": "SITUATION",
      "icon": "https://cdn.files.ordosszcs.com/20231113/bb0c2270bb6240dd9e5a6b2ef4b54ce4.png"
    }
  ]
  let result = []
  for(const i of baseStaff){
    if(i.community_id==query.communityId){
      staffs.push(i)
    }
  }
  for(const i of staffs){
    for(const j of baseStaffDict){
      if(i.id==j.staff_id&&j.type=='SITUATION'){
        i.base_dict_id = j.base_dict_id
        break
      }
    }
  }
  for(const i of staffs){
    const obj = {
      id:i.id,
      idCard:i.id_card,
      userName:i.user_name,
      tagIcon:'',
      tagName:'',
      base_dict_id:i.base_dict_id
    }
    for(const j of baseDict){
      if(i.base_dict_id==j.id){
        obj.tagIcon = j.icon
        obj.tagName = j.name
      }
    }
    result.push(obj)
  }
  if(query.tagId){
    let result2 = []
    for(const i of result){
      if(i.base_dict_id==query.tagId){
        result2.push(i)
      }
    }
    result = result2
  }
  return {
    familySitua,
    staffs:result
  }
}