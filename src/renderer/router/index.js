import Vue from 'vue'
import Router from 'vue-router'

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: require('@/views/Login').default
    },
    {
      path: '/menu',
      name: 'menu',
      component: require('@/views/Menu').default
    },
    {
      path: '/list',
      name: 'list',
      component: require('@/views/List').default
    },
    {
      path: '/display',
      name: 'display',
      component: require('@/views/display/index').default
    }
  ]
})
