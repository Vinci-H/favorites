

export default {
  data() {
    return {
      style: {
        transform: 'scaleY(1) scaleX(1) translate(-50%, -50%)',
      },
    }
  },
  mounted() {
    this.setScale()
    /* 窗口改变事件*/
    window.onresize = () => {
      this.setScale()
    }
  },
  activated() {
    
  },
  beforeDestroy() {

  },
  deactivated() {

  },
  methods: {
    getScale () {
      const w = window.innerWidth / 1920
      const h = window.innerHeight / 1080
      return w < h ? w : h
    },
    setScale () {
      const scale = this.getScale()
      this.style.transform = `scale(${ scale }) translate(-50%, -50%)`
    },
  }
}
