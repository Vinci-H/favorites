const state = {
    // 是否改变文件夹
    status:false,
  }
  
  // getters
  const getters = {
      // 获取状态
      getterStatus (state) {
          return status;
      }
  }
  
  // actions
  const actions = {
      // 改变状态
      setStatus (context,data) {
        context.commit('setStatus',data)
      },
  }
  
  // mutationsdd
  const mutations = {
    // 改变状态
    setStatus (state,data) {
     state.status = data;
    },
  }
  
  export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }